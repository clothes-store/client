import { useCallback } from 'react';
interface Error {
  path: string;
  type: string;
  message: string;
}
const useYupValidationResolver = (validationSchema: any) =>
  useCallback(
    async (data: any) => {
      try {
        const values = await validationSchema.validate(data, {
          abortEarly: false,
        });
        return {
          values,
          errors: {},
        };
      } catch (errors: any) {
        return {
          values: {},
          errors: errors.inner.reduce(
            (allErrors: Error[], currentError: Error) => {
              return {
                ...allErrors,
                [currentError.path]: {
                  type: currentError.type ?? 'validation',
                  message: currentError.message,
                },
              };
            },
            {},
          ),
        };
      }
    },
    [validationSchema],
  );
export { useYupValidationResolver };
