export const arraysEqual = (a: any[], b: any[]): boolean =>
  a.length === b.length && a.every((val, index) => val === b[index]);
