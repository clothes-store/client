class LocalStorageService {
  constructor() {}

  get(key: string) {
    return window.localStorage.getItem(key);
  }

  set(key: string, value: string) {
    window.localStorage.setItem(key, value);
  }

  clear(key: string) {
    window.localStorage.removeItem(key);
  }
}
export const localStorageService = new LocalStorageService();
