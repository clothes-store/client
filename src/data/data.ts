const sizes = [
  {
    id: 'size-1',
    text: 'XS',
  },
  {
    id: 'size-2',
    text: 'S',
  },
  {
    id: 'size-3',
    text: 'M',
  },
];
const categories = ['Men', 'Women'];

const colors = [
  {
    id: 'color-1',
    text: 'Blue',
  },
  {
    id: 'color-2',
    text: 'Red',
  },
  {
    id: 'color-3',
    text: 'Grey',
  },
  {
    id: 'color-4',
    text: 'Yellow',
  },
  {
    id: 'color-5',
    text: 'Black',
  },
  {
    id: 'color-6',
    text: 'White',
  },
  {
    id: 'color-7',
    text: 'Pink',
  },
];
const reviews = [
  {
    id: 'review-1',
    title: 'Review 1',
    description: 'lorem20',
    rating: 5,
    helpfulAmount: 2,
    uselessAmount: 10,
    author: 'Alicia',
  },
  {
    id: 'review-2',
    title: 'Review 2',
    description: 'lorem20',
    rating: 5,
    helpfulAmount: 102,
    uselessAmount: 12,
    author: 'Annie',
  },
  {
    id: 'review-3',
    title: 'Review 3',
    description: 'lorem20',
    rating: 5,
    helpfulAmount: 21,
    uselessAmount: 1,
    author: 'Tim',
  },
  {
    id: 'review-4',
    title: 'Review 4',
    description: 'lorem20',
    rating: 5,
    helpfulAmount: 2,
    uselessAmount: 0,
    author: 'Walker',
  },
];
export { sizes, colors, categories, reviews };
