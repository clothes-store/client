import React from 'react';
import { StarsRating } from '@/components/StarsRating/starsRating.interface';

const StarsRating = ({ rating }: StarsRating) => {
  const roundedRating = Math.round(rating);
  const arr = new Array(roundedRating).fill(0);
  return (
    <>
      {arr.map((el, i) => {
        return <img key={i} src='/star.svg' alt='Star Icon' />;
      })}
    </>
  );
};

export default StarsRating;
