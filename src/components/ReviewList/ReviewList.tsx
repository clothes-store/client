import React from 'react';
import styles from './reviewList.module.scss';
import { Button } from '@/components/UI/Button/Button';
import Review from '@/components/Review/Review';
import { reviews } from '@/data/data';

const ReviewList = () => {
  return (
    <div className={styles.wrapper}>
      <header className={styles.header}>
        <div className={styles.button}>
          <Button label='Write a review' variant='common' />
        </div>
        <span className={styles.amount}>3,205 reviews</span>
      </header>
      <div className={styles.container}>
        {reviews.map((review) => {
          return <Review key={review.id} review={review} />;
        })}
      </div>
    </div>
  );
};

export default ReviewList;
