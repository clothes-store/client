import styles from './checkboxList.module.scss';
import { memo, useState } from 'react';

interface CheckboxListProps {
  list: any[];
  checkedList?: any[];
  name: string;
  onChange: any;
}

const CheckboxList = memo(
  ({ list, checkedList = [], onChange }: CheckboxListProps) => {
    const [shownItemsLimit, setShownItemsNumber] = useState(3);
    return (
      <ul className={styles.wrapper}>
        {list.slice(0, shownItemsLimit).map((item) => (
          <li key={item} className={styles.item}>
            <input
              type='checkbox'
              id={item}
              value={item}
              checked={checkedList?.includes(item)}
              onChange={onChange}
            />
            <label className={styles.label} htmlFor={item}>
              <span className={styles.label__select}>
                <span className={styles.icon}>✔</span>
              </span>
              <span className={styles.label__text}>{item}</span>
            </label>
          </li>
        ))}
        {list.length > shownItemsLimit && (
          <div
            className={styles.more}
            onClick={() => setShownItemsNumber(list.length)}
          >
            <span className={styles.more__icon}>+</span>
            <span className={styles.more__text}>View more</span>
          </div>
        )}
      </ul>
    );
  },
);

CheckboxList.displayName = 'CheckboxList';
export default CheckboxList;
