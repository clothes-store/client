'use client';
import styles from './filter.module.scss';
import CheckboxList from '@/components/Filter/CheckboxList/CheckboxList';
import { Dropdown } from '@/components/UI/Dropdown/Dropdown';
import { RangeSlider } from '@/components/UI/RangeSlider/RangeSlider';
import {
  ChangeEvent,
  useCallback,
  useEffect,
  useLayoutEffect,
  useState,
} from 'react';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import ToggleSwitch from '@/components/UI/ToggleSwitch/ToggleSwitch';
import { categories } from '@/data/data';
import { products } from '@/mock/products';
import { Button } from '@/components/UI/Button/Button';

const sizes: string[] = [
  ...new Set(
    products
      .reduce((acc: string[], product) => {
        acc.push(...product.sizes);
        return acc;
      }, [])
      .flat(Infinity),
  ),
];
const colors: string[] = [
  ...new Set(
    products.reduce((acc: string[], product) => {
      acc.push(...product.colors);
      return acc;
    }, []),
  ),
];

const Filter = ({ prices }: { prices: any }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState<string | undefined>(
    '',
  );
  const [selectedSizes, setSelectedSizes] = useState<string[]>([]);
  const [selectedColors, setSelectedColors] = useState<string[]>([]);
  const [price, setPrice] = useState<{ min: number; max: number }>({
    min: prices.minPrice,
    max: prices.maxPrice,
  });
  const router = useRouter();
  const pathname = usePathname();
  const searchParams = useSearchParams();
  const [paramsState, setParamsState] = useState<any>(null);
  const changeSearchParams = () => {
    if (price) {
      const options = {
        cat: selectedCategory?.toLowerCase(),
        sizes: selectedSizes,
        colors: selectedColors,
        minPrice: price.min,
        maxPrice: price.max,
      };
      const params = new URLSearchParams();
      Object.entries(options).forEach(([parameter, value]) => {
        if (
          value !== null &&
          value !== undefined &&
          ((typeof value === 'string' && value.length !== 0) ||
            (Array.isArray(value) && value.length !== 0) ||
            typeof value === 'number')
        ) {
          params.set(parameter, String(value));
        }
      });
      setParamsState(params.toString() ?? '');
      router.push(pathname + '?' + params.toString(), { shallow: true });
    }
  };

  useLayoutEffect(() => {
    changeSearchParams();
  }, [
    pathname,
    router,
    searchParams,
    selectedCategory,
    selectedColors,
    selectedSizes,
    paramsState,
  ]);

  useEffect(() => {
    setSelectedCategory(
      categories.find((el) => el.toLowerCase() === searchParams.get('cat')) ??
        '',
    );
    setSelectedSizes(searchParams.get('sizes')?.split(',') || []);
    setSelectedColors(searchParams.get('colors')?.split(',') || []);
    setPrice({
      min: +(searchParams.get('minPrice') || prices.minPrice),
      max: +(searchParams.get('maxPrice') || prices.maxPrice),
    });
  }, [searchParams]);

  const handleSelectedSizes = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      const { value } = event.target as HTMLInputElement;
      if (selectedSizes.includes(value)) {
        setSelectedSizes(selectedSizes.filter((item) => item !== value));
      } else {
        setSelectedSizes([...selectedSizes, value]);
      }
    },
    [selectedSizes],
  );
  const handleSelectedColors = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      const { value } = event.target as HTMLInputElement;
      if (selectedColors.includes(value)) {
        setSelectedColors(selectedColors.filter((item) => item !== value));
      } else {
        setSelectedColors([...selectedColors, value]);
      }
    },
    [selectedColors],
  );
  return (
    <div className={styles.wrapper}>
      <div className={styles.title}>Filter</div>
      <div
        className={
          !isOpen ? styles.button : `${styles.button} ${styles.button_open}`
        }
      >
        <Button
          label='Filter'
          variant='filter'
          onClick={() => {
            setIsOpen((prevState) => !prevState);
          }}
        />
      </div>
      <div className={styles.container}>
        <Dropdown variant='text' title='Size' isOpen={true}>
          <CheckboxList
            list={sizes}
            name='sizes'
            onChange={handleSelectedSizes}
            checkedList={selectedSizes}
          />
        </Dropdown>
        <Dropdown variant='text' title='Color' isOpen={true}>
          <CheckboxList
            list={colors}
            name='colors'
            onChange={handleSelectedColors}
            checkedList={selectedColors}
          />
        </Dropdown>
        <Dropdown variant='text' title='Categories' isOpen={true}>
          {selectedCategory && (
            <ToggleSwitch
              id='toggle-unique'
              options={categories}
              defaultValue={selectedCategory}
              position='column'
              onChange={setSelectedCategory}
            />
          )}
        </Dropdown>
        <Dropdown variant='text' title='Price Range' isOpen={true}>
          {price && (
            <RangeSlider
              step={1}
              min={prices.minPrice}
              max={prices.maxPrice}
              value={price}
              onChange={setPrice}
              onMouseUp={changeSearchParams}
            />
          )}
        </Dropdown>
      </div>
    </div>
  );
};

export default Filter;
