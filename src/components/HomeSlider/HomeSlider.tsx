'use client';

import Image from 'next/image';
import styles from './index.module.scss';

import 'swiper/scss';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Autoplay, Mousewheel, Pagination } from 'swiper/modules';
import { PaginationOptions } from 'swiper/types';
import { Button } from '@/components/UI/Button/Button';

const Slider = () => {
  const pagination: PaginationOptions = {
    currentClass: styles.dots,
    clickableClass: styles.pagination,
    clickable: true,
    bulletClass: styles.dot,
    bulletActiveClass: styles.dot__active,
  };
  return (
    <>
      <Swiper
        loop={true}
        spaceBetween={10}
        pagination={pagination}
        modules={[Pagination, Mousewheel, Autoplay]}
        className={styles.wrapper}
        autoplay={{
          disableOnInteraction: false,
          delay: 3000,
          pauseOnMouseEnter: true,
        }}
        mousewheel={{
          forceToAxis: true,
        }}
        speed={1000}
        wrapperClass={styles.slide__wrapper}
      >
        <SwiperSlide className={styles.slide}>
          <div className={styles.hero__content}>
            <h1 className={styles.hero__title}>
              Step inside, for comfort and magic await you
            </h1>
            <Button label='Shop Now' variant='primary' />
          </div>

          <Image
            priority={true}
            src='/Photo2.jpg'
            alt='Home photo'
            width={1000}
            height={1000}
          />
        </SwiperSlide>
        <SwiperSlide className={styles.slide}>
          <div className={styles.hero__content}>
            <h1 className={styles.hero__title}>
              Step inside, for comfort and magic await you
            </h1>
            <Button label='Shop Now' variant='primary' />
          </div>
          <Image
            priority={true}
            src='/Photo.jpg'
            alt='Home photo'
            width={1000}
            height={1000}
            style={{
              width: '100%',
              height: '100%',
            }}
          />
        </SwiperSlide>
      </Swiper>
    </>
  );
};

export default Slider;
