'use client';
import styles from './header.module.scss';
import Logo from '@/components/Logo/Logo';
import UserIcon from '@/components/templates/Header/icons/userIcon';
import CartIcon from '@/components/templates/Header/icons/cartIcon';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { useState } from 'react';
import Cart from '@/components/Cart/Cart';
import { useAtomValue } from 'jotai';
import { cartAtom } from '@/features/store';

const Header = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [isCartOpen, setIsCartOpen] = useState(false);
  const currentRoute = usePathname();
  const navigationLinks = [
    {
      id: 'link-1',
      path: '/shop?cat=men',
      label: 'Men',
    },
    {
      id: 'link-2',
      path: '/shop?cat=women',
      label: 'Women',
    },
    {
      id: 'link-3',
      path: '/shop?cat=accessories',
      label: 'Accessories',
    },
    {
      id: 'link-4',
      path: '/about',
      label: 'About',
    },
  ];
  const cart = useAtomValue(cartAtom);
  return (
    <header className={styles.wrapper}>
      <div className={styles.container}>
        <Logo theme='white' />
        <button
          onClick={(e) => {
            e.stopPropagation();
            setIsOpen(!isOpen);
          }}
          className={styles.button}
        >
          <span></span>
        </button>
        <nav
          className={
            !isOpen
              ? styles.nav_main
              : `${styles.nav_main} ${styles.nav_main_open}`
          }
          onClick={(e) => {
            e.stopPropagation();
            const target = e.target as HTMLElement;
            if (!target.classList.contains(styles.nav_main__list)) {
              setIsOpen(false);
            }
          }}
        >
          <ul className={styles.nav_main__list}>
            {navigationLinks.map((link) => (
              <Link
                key={link.id}
                aria-disabled={link.path == currentRoute}
                className={
                  link.path !== currentRoute
                    ? styles.nav_main__item
                    : `${styles.nav_main__item} ${styles.nav_main__item_active}`
                }
                href={link.path}
              >
                {link.label}
              </Link>
            ))}
          </ul>
        </nav>
        <nav>
          <ul className={styles.nav_right}>
            <li>Support</li>
            <li>
              <Link href='/profile'>
                <UserIcon />
              </Link>
            </li>
            <li
              className={styles.cart_icon}
              onClick={() => setIsCartOpen((prevState) => !prevState)}
            >
              <CartIcon />
              {cart.length > 0 && <span>{cart.length}</span>}
            </li>
          </ul>
          <Cart isOpen={isCartOpen} setIsOpen={setIsCartOpen} />
        </nav>
      </div>
    </header>
  );
};

export default Header;
