import React from 'react';
import styles from './footer.module.scss';
import Logo from '@/components/Logo/Logo';
import Twitter from '@/components/templates/Footer/icons/twitter';
import Inst from '@/components/templates/Footer/icons/inst';
import Facebook from '@/components/templates/Footer/icons/facebook';
import { Input } from '@/components/UI/Input/Input';

const Footer = () => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.container}>
        <div className={styles.sopa}>
          <Logo theme='dark' />
          <p className={styles.info}>
            Stay informed about Sopa with our latest releases and founder news.
          </p>
          <div className={styles.input}>
            {/*<Input*/}
            {/*  type='email'*/}
            {/*  label='Email'*/}
            {/*  id='footer-email'*/}
            {/*  isControlled={false}*/}
            {/*/>*/}
          </div>
        </div>
        <ul className={styles.list}>
          <h5 className={styles.list_title}>Products</h5>
          <li className={styles.list_item}>Model 000</li>
          <li className={styles.list_item}>Model 000</li>
          <li className={styles.list_item}>Model 000</li>
          <li className={styles.list_item}>Model 000</li>
          <li className={styles.list_item}>Model 000</li>
          <li className={styles.list_item}>Model 000</li>
        </ul>
        <ul className={styles.list}>
          <h5 className={styles.list_title}>Support</h5>
          <li className={styles.list_item}>Help Center</li>
          <li className={styles.list_item}>FAQs</li>
          <li className={styles.list_item}>Order</li>
          <li className={styles.list_item}>Order Status</li>
          <li className={styles.list_item}>Returns & Exchanges</li>
          <li className={styles.list_item}>Contact US</li>
        </ul>
        <ul className={styles.list}>
          <h5 className={styles.list_title}>Everything Else</h5>
          <li className={styles.list_item}>Community</li>
          <li className={styles.list_item}>Why Sopa</li>
          <li className={styles.list_item}>About</li>
          <li className={styles.list_item}>Discount Program</li>
          <li className={styles.list_item}>Sopa Blog</li>
          <li className={styles.list_item}>Sopa Ambassadors</li>
        </ul>
        <ul className={styles.socials}>
          <li className={styles.socials_item}>
            <Twitter />
            <span>Twitter</span>
          </li>
          <li className={styles.socials_item}>
            <Inst />
            <span>Instagram</span>
          </li>
          <li className={styles.socials_item}>
            <Facebook />
            <span>Facebook</span>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Footer;
