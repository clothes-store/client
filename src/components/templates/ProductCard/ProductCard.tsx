import { ProductCardProps } from '@/components/templates/ProductCard/productCard.interface';
import styles from './productCard.module.scss';
import Image from 'next/image';
import { Button } from '@/components/UI/Button/Button';
import Link from 'next/link';

const ProductCard = ({
  id,
  isButton = false,
  image,
  title,
  category,
  price,
  className,
}: ProductCardProps) => {
  const productHref = `/${id}`;

  return (
    <div
      id={`product-id=${id}`}
      className={!className ? styles.wrapper : `${styles.wrapper} ${className}`}
    >
      <div className={styles.image}>
        <Image
          src={
            `${process.env.NEXT_PUBLIC_SERVER_BASE_URL}/${image[0]}` ||
            'https://pixabay.com/get/gd5e498ec69e25fa90c74d6c1759af9c221031bd4168e28f8e8caebcfcd5f581ca18ed8b84022bd0dc2811b863e7a4201_1280.jpg'
          }
          alt={title}
          width={0}
          height={0}
          sizes='100vw'
          style={{ width: '100%', height: 'auto' }}
        />
      </div>
      <div className={styles.footer}>
        <div className={styles.info}>
          <Link href={productHref} className={styles.title}>
            {title}
          </Link>
          <span className={styles.category}>{category}</span>
        </div>
        {isButton ? (
          <Button label='Shop Now' variant='primary' />
        ) : (
          <span className={styles.price}>${price}</span>
        )}
      </div>
    </div>
  );
};

export default ProductCard;
