export interface ProductCardProps {
  id: string;
  isButton: boolean;
  image: string;
  title: string;
  category: string;
  price: number;
  className?: string;
  size?: string;
  color?: string;
}
