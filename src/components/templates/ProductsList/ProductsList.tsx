import styles from './productsList.module.scss';
import { Button } from '@/components/UI/Button/Button';
import ProductCard from '@/components/templates/ProductCard/ProductCard';
import Link from 'next/link';

const getProducts = async () => {
  const res = await fetch(
    `${process.env.NEXT_PUBLIC_SERVER_BASE_URL}/products`,
  );
  return await res.json();
};
const ProductsList = async () => {
  const products = await getProducts();
  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <h3 className={styles.title}>Explore Model 000</h3>
        <Link href='/shop' className={styles.link}>
          Shop Now
        </Link>
      </div>
      <div className={styles.container}>
        {products.map((product: any) => {
          return (
            <div key={product.id} className={styles.item}>
              <ProductCard
                id={product.id}
                isButton={false}
                image={product.images}
                title={product.title}
                category={product.category}
                price={product.price}
              />
            </div>
          );
        })}
      </div>
      <Button label='Shop Now' variant='primary' />
    </div>
  );
};

export default ProductsList;
