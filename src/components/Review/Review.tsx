'use client';
import styles from './review.module.scss';
import StarsRating from '@/components/StarsRating/StarsRating';
import ThumbUp from '@/icons/thumb_up';
import ThumbDown from '@/icons/thumb_down';
import { useState } from 'react';

const Review = ({ review }: any) => {
  const [isHelpful, setIsHelpful] = useState<null | boolean>(null);
  const [amount, setAmount] = useState({
    helpful: review.helpfulAmount,
    useless: review.uselessAmount,
  });
  return (
    <article className={styles.wrapper}>
      <div className={styles.main}>
        <h1 className={styles.author}>{review.author}</h1>
        <div className={styles.reviewing}>
          <div className={styles.reviewing_title}>Reviewing</div>
          <div className={styles.reviewing_modelName}>Model 000:Gray</div>
        </div>
      </div>
      <div className={styles.content}>
        <div className={styles.stars}>
          <StarsRating rating={review.rating} />
        </div>
        <h1 className={styles.summary}>Super comfy and holding up nicely</h1>
        <p className={styles.text}>
          These shoes are my go-to for comfort and durability as a busy teacher
          and devoted mom of two young children. I wear them to work nearly
          every day and they still hold up incredibly well, earning a glowing
          5-star review despite a stubborn coffee stain.
        </p>
        <div className={styles.helpful}>
          <span className={styles.helpful_title}>Was this helpful?</span>
          <input
            type='checkbox'
            id='helpful'
            checked={isHelpful || false}
            onChange={() => {
              setIsHelpful(true);
              if (isHelpful === null) {
                setAmount((prevState) => {
                  return { ...prevState, helpful: prevState.helpful + 1 };
                });
              } else if (!isHelpful) {
                setAmount((prevState) => {
                  return {
                    useless: prevState.useless - 1,
                    helpful: prevState.helpful + 1,
                  };
                });
              }
            }}
          />
          <label htmlFor='helpful'>
            <ThumbUp />
            <span className={styles.amount}>{amount.helpful}</span>
          </label>
          <input
            type='checkbox'
            id='useless'
            checked={isHelpful === null ? false : !isHelpful}
            onChange={() => {
              setIsHelpful(false);
              if (isHelpful === null) {
                setAmount((prevState) => {
                  return { ...prevState, useless: prevState.useless + 1 };
                });
              } else if (isHelpful) {
                setAmount((prevState) => {
                  return {
                    useless: prevState.useless + 1,
                    helpful: prevState.helpful - 1,
                  };
                });
              }
            }}
          />
          <label htmlFor='useless'>
            <ThumbDown />
            <span className={styles.amount}>{amount.useless}</span>
          </label>
        </div>
      </div>
    </article>
  );
};

export default Review;
