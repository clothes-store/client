'use client';
import React, { useEffect, useState } from 'react';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import styles from '@/app/[id]/specificProduct.module.scss';
import Select from '@/components/UI/Select/Select';
import StarsRating from '@/components/StarsRating/StarsRating';
import SpecificProductSlider from '@/components/SpeficifProductSlider/SpecificProductSlider';
import { Product } from '@/entities/Product';
import ProductButton from '@/components/SpecificProductPage/Button';

const SpecificProductPage = (data: { data: Product }) => {
  const [product, setProduct] = useState<Product>(data.data);
  const [selectedSize, setSelectedSize] = useState<string | null>(null);

  const searchParams = useSearchParams();
  const router = useRouter();
  const pathname = usePathname();

  const size = searchParams.get('size');

  const changePath = (option: any) => {
    router.push(pathname + '?' + `size=${option || product.sizes[0].size}`, {
      shallow: true,
    });
    setSelectedSize(option);
  };
  useEffect(() => {
    router.push(
      pathname + '?' + `size=${selectedSize || product.sizes[0].size}`,
      { shallow: true },
    );
  }, [router, pathname, product]);
  return (
    <div className={styles.wrapper}>
      {product ? (
        <div className={styles.container}>
          <SpecificProductSlider slides={product?.images} />
          <div className={styles.content}>
            <div className={styles.header}>
              <div className={styles.header_container}>
                <span className={styles.title}>Sopa Laces</span>
                <div className={styles.content_rating}>
                  <div className={styles.stars}>
                    <StarsRating rating={4.7} />
                  </div>
                  <div className={styles.reviews}>
                    <span>4.7</span>
                    <span>(3205)</span>
                  </div>
                </div>
              </div>
              <span className={styles.price}>$12</span>
            </div>
            <div className={styles.colors}>
              <span className={styles.colors_title}>Color:{product.color}</span>
              {/*<div className={styles.colors_container}>*/}
              {/*  {product.colors.map((color: any, i: number) => {*/}
              {/*    return (*/}
              {/*      <div*/}
              {/*        key={i}*/}
              {/*        className={`${styles.colors_item} ${*/}
              {/*          searchParams.get('color') === color*/}
              {/*            ? styles.colors_item__active*/}
              {/*            : ''*/}
              {/*        }`}*/}
              {/*        style={{*/}
              {/*          backgroundColor: color || searchParams.get('color'),*/}
              {/*        }}*/}
              {/*      ></div>*/}
              {/*    );*/}
              {/*  })}*/}
              {/*</div>*/}
            </div>
            <div className={styles.sizes}>
              <div className={styles.sizes_header}>
                <span className={styles.sizes_title}>Size</span>
                <span className={styles.sizes_guide}>Size Guide</span>
              </div>
              <Select
                onCLick={changePath}
                options={product.sizes.map((size: any) => size.size)}
                label={size || product.sizes[0].size}
              />
            </div>
            <ProductButton product={product} />
            <h1 className={styles.freeShipping}>
              Free shipping on orders over $200
            </h1>
            <p className={styles.description}>
              Make putting on your shoes as easy as pulling on a T-shirt with
              Sopa Stretch Laces - their unique stretching properties adapt to
              your foot's movements and prevent getting untied, and now you can
              customize with multiple vibrant colors in the right size for your
              Sopa shoe
            </p>
          </div>
        </div>
      ) : (
        <div>Loading...</div>
      )}
    </div>
  );
};

export default SpecificProductPage;
