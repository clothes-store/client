import React from 'react';
import styles from '@/app/[id]/specificProduct.module.scss';
import { Button } from '@/components/UI/Button/Button';
import { addProduct } from '@/features/actions';
import { Product } from '@/entities/Product';
import { useAtom } from 'jotai/index';
import { cartAtom } from '@/features/store';
import { useSearchParams } from 'next/navigation';

const ProductButton = ({ product }: { product: Product }) => {
  const [cartState, setCart] = useAtom(cartAtom);

  const searchParams = useSearchParams();
  const handleAddingToCart = async () => {
    const productDTO = {
      id: product.id,
      title: product.title,
      price: product.price,
      color: product.color,
      size: searchParams.get('size'),
      image: product.images[0],
      amount: 1,
    };
    const newCart = addProduct(cartState, productDTO);
    newCart &&
      setCart(() => {
        return [...newCart];
      });
  };
  return (
    <div className={styles.button}>
      <Button
        label='Add to bag'
        variant='common'
        onClick={() => handleAddingToCart()}
      />
    </div>
  );
};

export default ProductButton;
