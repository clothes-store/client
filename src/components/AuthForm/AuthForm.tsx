'use client';
import { Children, createElement, useState } from 'react';
import { usePathname, useRouter } from 'next/navigation';

import { FormProvider, useForm } from 'react-hook-form';
import { object, ObjectSchema, string } from 'yup';
import { useYupValidationResolver } from '@/utils/useYupValidationResolver.hook';

import { Props, FormData } from '@/components/AuthForm/AuthForm.interface';
import { localStorageService } from '@/utils/LocalStorage.service';

import styles from './form.module.scss';

const AuthForm = ({ defaultValues, children }: Props) => {
  const router = useRouter();
  const [error, setError] = useState<any>(null);
  const pathname = usePathname().split('/');
  const action = pathname[pathname.length - 1];

  const validationSchema: ObjectSchema<FormData> = object({
    email: string()
      .matches(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        {
          message: 'Must be email!',
        },
      )
      .required('Email is required'),
    password: string()
      .matches(/[A-Z]/, 'Password must contain at least one uppercase letter')
      .matches(/[a-z]/, 'Password must contain at least one lowercase letter')
      .matches(/[0-9]/, 'Password must contain at least one digit')
      .matches(
        /[!@#$%^&*(),.?":{}|<>_]/,
        'Password must contain at least one special character',
      )
      .required('Password is required!')
      .min(8, 'Password must be at least 8 characters long'),
  });
  const resolver = useYupValidationResolver(validationSchema);
  const methods = useForm({ defaultValues, resolver, mode: 'onTouched' });
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = methods;
  const url = `${process.env.NEXT_PUBLIC_SERVER_BASE_URL}/auth/${action}`;
  const handleFormSubmitting = async (data: FormData) => {
    try {
      const res = await fetch(url, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Content-type': 'application/json',
        },
        credentials: 'include',
      });
      const fetchedData = await res.json();
      if (res.ok) {
        router.push('/profile');
        localStorageService.set('accessToken', fetchedData.accessToken);
      } else {
        setError(() => fetchedData);
      }
    } catch (e) {
      throw e;
    }
  };
  return (
    <FormProvider {...methods}>
      <form
        className={styles.wrapper}
        onSubmit={handleSubmit(handleFormSubmitting)}
      >
        {error && <div className={styles.error}>{error.message}</div>}
        {Children.map(children, (child) => {
          return child.props.name
            ? createElement(child.type, {
                ...{
                  ...child.props,
                  type: child.props.type,
                  register: register,
                  key: child.props.name,
                  name: child.props.name,
                  isError: !!errors[child.props.name],
                  error: errors[child.props.name],
                },
              })
            : child;
        })}
      </form>
    </FormProvider>
  );
};

export default AuthForm;
