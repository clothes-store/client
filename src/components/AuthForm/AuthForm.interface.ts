export interface FormData {
  email: string;
  password: string;
}
export interface Props {
  defaultValues?: any;
  children: any;
  onSubmit?: any;
  action: string;
}
