'use client';
import React from 'react';
import styles from './loader.module.scss';

const Loader = () => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.container}>
        <div className={styles.black_circle}></div>
        <div className={styles.white_circle}></div>
        <div className={styles.transparent_circle}></div>
      </div>
    </div>
  );
};

export default Loader;
