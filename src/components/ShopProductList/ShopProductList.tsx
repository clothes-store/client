import React from 'react';
import styles from './index.module.scss';
import ProductCard from '@/components/templates/ProductCard/ProductCard';

interface Product {
  id: string;
  title: string;
  images: string[];
  category: string;
  price: number;
  sizes: string[];
  colors: string[];
}

interface ShopProductListProps {
  searchParams: any;
}
async function getData(paramsStr: string) {
  const url = `${process.env.NEXT_PUBLIC_SERVER_BASE_URL}/products?${paramsStr}`;
  const res = await fetch(url);
  return await res.json();
}

const ShopProductList: React.FC<ShopProductListProps> = async ({
  searchParams,
}) => {
  const paramsStr = Object.entries(searchParams)
    .map((arr) => arr.join('='))
    .join('&');
  const products = await getData(paramsStr);
  return (
    <div className={styles.container}>
      {products.length !== 0 ? (
        products.map((product: any) => {
          return (
            <div className={styles.product} key={product.id}>
              <ProductCard
                id={product.id}
                isButton={false}
                image={product.images}
                title={product.title}
                category={product.category}
                price={product.price}
              />
            </div>
          );
        })
      ) : (
        <div>Sorry, items with your filter cannot be found</div>
      )}
    </div>
  );
};

export default ShopProductList;
