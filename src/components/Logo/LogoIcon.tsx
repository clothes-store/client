import React from 'react';
import styles from './logoIcon.module.scss';

interface LogoIcon {
  theme: 'dark' | 'white';
}
const LogoIcon = ({ theme }: LogoIcon) => {
  return (
    <svg
      width='41'
      height='28'
      viewBox='0 0 41 28'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        className={
          theme === 'white' ? styles.icon_stroke : styles.icon_stroke_dark
        }
        d='M39.7023 13.8401C39.7023 20.8606 33.9822 26.5601 26.916 26.5601C19.8498 26.5601 14.1298 20.8606 14.1298 13.8401C14.1298 6.81959 19.8498 1.12012 26.916 1.12012C33.9822 1.12012 39.7023 6.81959 39.7023 13.8401Z'
        strokeWidth='2'
      />
      <path
        className={
          theme === 'white' ? styles.icon_stroke : styles.icon_stroke_dark
        }
        d='M33.1374 13.8401C33.1374 20.8606 27.4173 26.5601 20.3511 26.5601C13.2849 26.5601 7.56488 20.8606 7.56488 13.8401C7.56488 6.81959 13.2849 1.12012 20.3511 1.12012C27.4173 1.12012 33.1374 6.81959 33.1374 13.8401Z'
        strokeWidth='2'
      />
      <path
        className={theme === 'white' ? styles.icon_fill : styles.icon_fill_dark}
        d='M27.5725 13.8401C27.5725 21.4175 21.4002 27.5601 13.7863 27.5601C6.17232 27.5601 0 21.4175 0 13.8401C0 6.26277 6.17232 0.120117 13.7863 0.120117C21.4002 0.120117 27.5725 6.26277 27.5725 13.8401Z'
      />
    </svg>
  );
};

export default LogoIcon;
