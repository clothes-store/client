import React from 'react';
import LogoIcon from '@/components/Logo/LogoIcon';
import styles from './logo.module.scss';
import Link from 'next/link';
interface Logo {
  theme: 'dark' | 'white';
}

const Logo = ({ theme }: Logo) => {
  return (
    <Link href='/' data-theme={theme} className={styles.wrapper}>
      <LogoIcon theme={theme} />
      <h1 className={styles.text}>Sopa</h1>
    </Link>
  );
};

export default Logo;
