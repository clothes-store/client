'use client';
import styles from './dropdown.module.scss';
import React, { ReactElement, useState, ReactNode } from 'react';
interface Dropdown {
  children: ReactElement | ReactNode;
  variant: 'button' | 'text';
  title: string;
  isOpen: boolean;
}

export const Dropdown = ({
  variant,
  title,
  isOpen = false,
  children,
}: Dropdown) => {
  const [open, setOpen] = useState(isOpen || false);
  const variantClassName = styles[`${variant}_variant`];
  return (
    <div
      datatype='dropdown'
      className={`${styles.wrapper} ${variantClassName}`}
    >
      <div
        className={
          !open ? styles.content : `${styles.content} ${styles.content_open}`
        }
        onClick={() => setOpen(!open)}
      >
        {title}
        {variant === 'text' && <span>{open ? '-' : '+'}</span>}
      </div>
      <div
        className={
          !open ? styles.modal : `${styles.modal} ${styles.modal_open}`
        }
      >
        {children}
      </div>
    </div>
  );
};
