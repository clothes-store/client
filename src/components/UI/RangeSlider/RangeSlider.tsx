'use client';
import { ChangeEvent, useEffect, useState } from 'react';
import { RangeSliderProps } from '@/components/UI/RangeSlider/rangeSlider.interface';
import './index.scss';
import styles from './rangeSlider.module.scss';

export const RangeSlider = ({
  min,
  max,
  value,
  step,
  onChange,
  onMouseUp,
}: RangeSliderProps) => {
  const [minValue, setMinValue] = useState(value ? value.min : min);
  const [maxValue, setMaxValue] = useState(value ? value.max : max);
  useEffect(() => {
    if (value) {
      setMinValue(value.min);
      setMaxValue(value.max);
    }
  }, [value]);

  const handleMinChange = (e: ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    const newMinVal = Math.min(+e.target.value, maxValue - step);
    if (!value) setMinValue(newMinVal);
    onChange({ min: newMinVal, max: maxValue });
    setMinValue(newMinVal);
  };

  const handleMaxChange = (e: ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    const newMaxVal = Math.max(+e.target.value, minValue + step);
    if (!value) setMaxValue(newMaxVal);
    onChange({ min: minValue, max: newMaxVal });
  };

  const minPos = ((minValue - min) / (max - min)) * 100;
  const maxPos = ((maxValue - min) / (max - min)) * 100;

  return (
    <div className='wrapper'>
      <div className='input-wrapper'>
        <input
          className='input'
          type='range'
          value={minValue}
          min={min}
          max={max}
          step={step}
          onChange={handleMinChange}
          onMouseUp={onMouseUp}
        />
        <input
          className='input'
          type='range'
          value={maxValue}
          min={min}
          max={max}
          step={step}
          onChange={handleMaxChange}
          onMouseUp={onMouseUp}
        />
      </div>

      <div className='control-wrapper'>
        <div className='control' style={{ left: `${minPos}%` }} />
        <div className='rail'>
          <div
            className='inner-rail'
            style={{ left: `${minPos}%`, right: `${100 - maxPos}%` }}
          />
        </div>
        <div className='control' style={{ left: `${maxPos}%` }} />
      </div>
      <div className={styles.values}>
        <span className={styles.values__input}>
          <span>$</span>
          <span>{minValue}</span>
        </span>
        <span className={styles.values__input}>
          <span>$</span>
          <span>{maxValue}</span>
        </span>
      </div>
    </div>
  );
};
