export interface RangeSliderProps {
  step: number;
  min: number;
  max: number;
  value?: {
    min: number;
    max: number;
  };
  onChange?: any;
  onMouseUp?: any;
}
