import { ButtonInterface } from '@/components/UI/Button/button.interface';
import styles from './button.module.scss';
import FilterIcon from '@/components/UI/Button/icons/Filter.icon';
import BackArrowIcon from '@/components/UI/Button/icons/BackArrow.icon';
import DownArrowIcon from '@/components/UI/Button/icons/DownArrow.icon';

export const Button = ({
  label,
  type,
  variant,
  onClick,
  className,
  img,
  isDisabled = false,
}: ButtonInterface) => {
  const defaultClassName = styles[`${variant}`];

  return (
    <button
      className={
        !className ? defaultClassName : `${defaultClassName} ${className}`
      }
      onClick={onClick}
      disabled={isDisabled}
    >
      {img && <img src={img} alt={label} />}
      {variant === 'filter' && <FilterIcon className={styles.icon} />}
      {variant === 'back' && <BackArrowIcon className={styles.icon} />}
      {variant === 'date' && <DownArrowIcon className={styles.icon} />}
      <span>{label}</span>
    </button>
  );
};
