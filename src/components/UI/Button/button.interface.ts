import { MouseEventHandler } from 'react';

export interface ButtonInterface {
  label: string;
  variant: 'primary' | 'date' | 'filter' | 'common' | 'back';
  type?: 'submit' | 'reset';
  onClick?: MouseEventHandler;
  className?: string;
  img?: string;
  isDisabled?: boolean;
}

export interface ButtonIconInterface {
  className: string;
}
