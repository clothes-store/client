import React from 'react';
import { ButtonIconInterface } from '@/components/UI/Button/button.interface';

const BackArrowIcon = ({ className }: ButtonIconInterface) => {
  return (
    <svg
      width='24'
      height='24'
      viewBox='0 0 24 24'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        className={className}
        d='M19 12L5 12'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        className={className}
        d='M12 19L5 12L12 5'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  );
};

export default BackArrowIcon;
