import React from 'react';
import { ButtonIconInterface } from '@/components/UI/Button/button.interface';
const DownArrowIcon = ({ className }: ButtonIconInterface) => {
  return (
    <svg
      width='14'
      height='8'
      viewBox='0 0 14 8'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        className={className}
        d='M1 1L7 7L13 1'
        stroke='#02021D'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  );
};

export default DownArrowIcon;
