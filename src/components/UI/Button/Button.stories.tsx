import type { Meta, StoryObj } from '@storybook/react';

import { Button } from './Button';

const meta: Meta<typeof Button> = {
  component: Button,
  tags: ['autodocs'],
  argTypes: {
    variant: {
      options: ['common', 'primary', 'filter', 'date'],
      control: { type: 'select' },
    },
    isDisabled: {
      type: 'boolean',
      control: {
        type: 'boolean',
      },
      defaultValue: false,
      description: 'The disabling button flag',
    },
  },
};

export default meta;
type Story = StoryObj<typeof Button>;

export const Common: Story = {
  args: {
    label: 'Common Button',
    variant: 'common',
  },
};
export const Primary: Story = {
  args: {
    label: 'Primary Button',
    variant: 'primary',
  },
};
export const Date: Story = {
  args: {
    label: 'Date Button',
    variant: 'date',
  },
};
export const Filter: Story = {
  args: {
    label: 'Filter Button',
    variant: 'filter',
  },
};
