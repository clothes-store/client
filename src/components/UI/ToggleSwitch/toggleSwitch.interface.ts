export interface ToggleSwitchProps {
  id: string;
  options: string[];
  defaultValue: string;
  position?: 'row' | 'column';
  onChange?: any;
}
