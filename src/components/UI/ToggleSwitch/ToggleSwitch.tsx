import React, { memo, useEffect, useState } from 'react';
import { ToggleSwitchProps } from '@/components/UI/ToggleSwitch/toggleSwitch.interface';
import styles from './toggleSwitch.module.scss';
import { useSearchParams } from 'next/navigation';

const ToggleSwitch = memo(
  ({ options, defaultValue, position, onChange }: ToggleSwitchProps) => {
    const searchParams = useSearchParams();
    const [activeOption, setActiveOption] = useState(defaultValue);
    const handleToggle = (option: string) => {
      onChange && onChange(option);
      setActiveOption(option);
    };
    const positionCLass = styles[`${position}`];

    useEffect(() => {}, [searchParams]);
    return (
      <div
        className={
          !position ? styles.wrapper : `${styles.wrapper} ${positionCLass}`
        }
      >
        {options.map((option, i) => {
          return (
            <div
              className={
                defaultValue === option
                  ? `${styles.label} ${styles.label_active}`
                  : styles.label
              }
              key={i}
            >
              <input
                className={styles.input}
                key={i}
                checked={option === defaultValue}
                id={`toggle-${option}`}
                type='radio'
                name='toggle'
                onChange={() => handleToggle(option)}
              />
              <label htmlFor={`toggle-${option}`}>{option}</label>
            </div>
          );
        })}
      </div>
    );
  },
);
ToggleSwitch.displayName = 'ToggleSwitch';
export default ToggleSwitch;
