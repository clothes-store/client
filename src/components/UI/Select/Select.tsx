import { SelectProps } from '@/components/UI/Select/select.interface';
import styles from './select.module.scss';
import { useLayoutEffect, useState } from 'react';
import ArrowDown from '@/components/UI/Select/icons/arrowDown';
import ArrowUp from '@/components/UI/Select/icons/arrowUp';

const Select = ({ options, label, onCLick }: SelectProps) => {
  const [isOpen, setIsOpen] = useState(false);
  const [activeOption, setActiveOption] = useState<null | string>(null);

  useLayoutEffect(() => {
    const handler = (event: MouseEvent) => {
      const target = event.target as HTMLElement;
      if (!target.dataset.select) {
        setIsOpen(false);
      } else if (target.dataset.select === 'option') {
        setActiveOption(target.innerText);
        setIsOpen(false);
        onCLick(target.innerText);
      }
    };
    window.addEventListener('click', handler);
    return () => {
      window.removeEventListener('click', handler);
    };
  }, [onCLick]);
  return (
    <div
      data-select={true}
      className={styles.wrapper}
      onClick={() => setIsOpen((prevState) => !prevState)}
    >
      <div data-select={true} className={styles.container}>
        <div data-select={true} className={styles.content}>
          {activeOption ? activeOption : label}
        </div>
        <button data-select={true} className={styles.button}>
          {!isOpen ? (
            <ArrowDown className={styles.icon} />
          ) : (
            <ArrowUp className={styles.icon} />
          )}
        </button>
      </div>
      <ul
        data-select={true}
        className={isOpen ? `${styles.list} ${styles.list_open}` : styles.list}
      >
        {options.map((option, i) => {
          return (
            <li
              data-select='option'
              key={i}
              className={
                option !== activeOption
                  ? styles.list_item
                  : `${styles.list_item} ${styles.list_item_active}`
              }
            >
              {option}
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default Select;
