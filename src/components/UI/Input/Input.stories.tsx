import { Input } from '@/components/UI/Input/Input';
import { Meta, type StoryObj } from '@storybook/react';

const meta: Meta<typeof Input> = {
  component: Input,
  tags: ['autodocs'],
  argTypes: {
    type: {
      options: ['email', 'text', 'password', 'number'],
      control: { type: 'select' },
    },
    isCorrect: {
      options: [true, false, null],
      control: {
        type: 'radio',
      },
    },
  },
};
export default meta;
type Story = StoryObj<typeof Input>;

export const Email: Story = {
  args: {
    label: 'Email',
    type: 'email',
    value: '',
    id: 'email',
    isCorrect: null,
  },
};
export const Password: Story = {
  args: {
    label: 'Password',
    type: 'password',
    value: '',
    id: 'password',
    isCorrect: null,
  },
};
