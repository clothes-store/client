'use client';
import { useState } from 'react';
import { InputProps } from '@/components/UI/Input/input.interface';
import styles from './input.module.scss';
import QuestionIcon from '@/components/UI/Input/icons/questionIcon';
import { useFormContext } from 'react-hook-form';
import Eye from '@/icons/eye';
import EyeClosed from '@/icons/eyeClosed';

export const Input = ({
  id,
  label,
  isError,
  isValid,
  isControlled,
  isAdvise,
  name,
  error,
  ...props
}: any) => {
  const { register, watch } = useFormContext();
  const value = watch(name);
  const isEmpty = value ? value.length === 0 : true;
  const className = isEmpty
    ? styles.field
    : isError
      ? `${styles.field} ${styles.field_error}`
      : `${styles.field} ${styles.field_valid}`;
  const [isPasswordShown, setIsPasswordShown] = useState(false);
  return (
    <fieldset className={styles.wrapper}>
      <div className={styles.label__wrapper}>
        <input
          placeholder={name}
          className={className}
          id={name}
          type={name === 'password' && isPasswordShown ? 'string' : name}
          {...register(name)}
        />
        <label className={styles.label} htmlFor={name}>
          {name}
        </label>
        {name === 'password' && (
          <div
            className={styles.icon}
            onClick={() => {
              setIsPasswordShown((prevState) => !prevState);
            }}
          >
            {isPasswordShown ? <Eye /> : <EyeClosed />}
          </div>
        )}
      </div>
      {isError && <div className={styles.error}>{error.message}</div>}
      {isAdvise && (
        <div className={styles.button}>
          <button className={styles.icon_wrapper}>
            <QuestionIcon className={styles.icon} />
          </button>
          <div className={styles.modal}>
            Much of the time, we’ll find ourselves dealing with objects that
            might have a property set. In those cases, we can mark those
            properties as optional by adding a question mark (?) to the end of
            their names.
          </div>
        </div>
      )}
    </fieldset>
  );
};
