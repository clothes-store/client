import { ChangeEventHandler } from 'react';

export interface InputProps {
  type: 'email' | 'text' | 'password' | 'number';
  label: string;
  id: string;
  className?: string;
  isCorrect?: boolean | null;
  isAdvise?: boolean;
  isControlled: boolean;
  value?: any;
  onChange?: ChangeEventHandler<HTMLInputElement>;
}

export interface InputIconProps {
  className: string;
}
