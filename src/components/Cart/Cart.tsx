'use client';
import styles from './cart.module.scss';
import { Button } from '@/components/UI/Button/Button';
import { useEffect, useState, MouseEvent } from 'react';
import CartElement from '@/components/CartElement/CartElement';
import { useAtomValue } from 'jotai';
import { cartAtom } from '@/features/store';
import { CartProduct } from '@/entities/CartProduct';

const Cart = ({ isOpen, setIsOpen }: { isOpen: boolean; setIsOpen: any }) => {
  const [isCartOpen, setIsCartOpen] = useState<null | boolean>(null);
  const cart = useAtomValue(cartAtom);
  useEffect(() => {
    setIsCartOpen(() => isOpen);
  }, [isOpen]);
  const className = isCartOpen
    ? `${styles.wrapper} ${styles.wrapper__open}`
    : styles.wrapper;
  const totalPrice =
    cart.length > 0 &&
    cart.reduce((acc: number, el: CartProduct) => {
      acc += el.price * el.amount;
      return acc;
    }, 0);
  return (
    <section
      id='wrapper'
      className={className}
      onClick={(event: MouseEvent<HTMLDivElement>) => {
        const target = event.target as HTMLDivElement;
        if (target.id === 'wrapper') {
          setIsCartOpen(false);
          setIsOpen(false);
        }
      }}
    >
      <div className={styles.container}>
        <header className={styles.header}>
          <h1 className={styles.header_title}>Cart</h1>
          <button
            className={styles.header_button}
            onClick={() => {
              setIsCartOpen(false);
              setIsOpen(false);
            }}
          >
            X
          </button>
        </header>
        <main className={styles.main}>
          {cart.map((product: CartProduct) => {
            return (
              <CartElement
                product={product}
                key={product.id + '' + product.size}
              />
            );
          })}
        </main>
        {totalPrice && (
          <footer className={styles.footer}>
            <div className={styles.footer_container}>
              <h1 className={styles.footer_title}>Total</h1>
              <span className={styles.footer_subtotal}>${totalPrice}</span>
            </div>
            <Button label='Continue to Checkout' variant='common' />
          </footer>
        )}
      </div>
    </section>
  );
};

export default Cart;
