'use client';
import styles from './cartElement.module.scss';
import { useAtom } from 'jotai';
import { cartAtom } from '@/features/store';
import { CartProduct } from '@/entities/CartProduct';
import { removeProduct } from '@/features/actions';
import { Button } from '@/components/UI/Button/Button';
import Image from 'next/image';
interface props {
  product?: any;
  increment?: any;
  decrement?: any;
}
const CartElement = ({ product }: props) => {
  const [cart, setCart] = useAtom(cartAtom);
  const handleCartProductAmount = (action: 'increment' | 'decrement') => {
    const mathSymbol = action === 'increment' ? 1 : -1;
    const existedIndex = cart.findIndex((el: CartProduct) => {
      return el.id === product.id && product.size === el.size;
    });
    const newCart = cart.map((el: CartProduct, i: number) => {
      if (i === existedIndex) {
        return { ...el, amount: el.amount - mathSymbol };
      }
      return el;
    });
    setCart(newCart);
  };
  return (
    <div className={styles.wrapper}>
      <div className={styles.image}>
        <Image
          src={
            `${process.env.NEXT_PUBLIC_SERVER_BASE_URL}/${product?.image}` ||
            'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhIQEhIRFRIVFRAYERUWFRkTEhsYGBUXFxgVGRUYHS4gGBolHRcVITYhJSorLi4uGB8zODMsNygtLysBCgoKDg0OFQ8PFysZFR0tNy0tLS0tKystKy0rLS0rKzcrLS0tLSsrNy0tKy0rLS0tLS0tKystKys3KysrKzc3K//AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAgQFBgcDAf/EAEIQAAIBAgIHBQUFBAkFAAAAAAABAgMRBCEFEjFBUWFxBiKBkfAHEzKhsUJScsHRkpTh8RQjRVVigqLS0xczg7LC/8QAFgEBAQEAAAAAAAAAAAAAAAAAAAEC/8QAGREBAQEBAQEAAAAAAAAAAAAAABEBIVES/9oADAMBAAIRAxEAPwDsQAKgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAxuC0vGrOtGFmqU9Rvi0k387rwMjGV1dAfQAAAPPEVlBaz5W4tvYgPQHlCrlrNW8b26nomB9AAAAAAAAAAAAAAAAAAAAAAAAAAAoacxbpUKk4/HbVp/jk9WPzd/Avmu9q8StbD0uM/eS6RyXzb8gMf2SoOnUlBp9+nmn96Dz+Ul5G04F93V4GFs1KFaKvZptLa1ZxkvJX8DJ0aqu5Rd4PNNbnvTRvcGQBWeIt9qPiVq+Pa2NeVvr+hmC7ia6hFzlsXqxRpz12py8FtUdvhfmVlJzfez5PZ5P8/IydOKtsu91/WRZBCFZXsmrvYrq/kQdPUb1XJXecdZ6t2ruyeSva9v1zsU5zc3nDUy1VZxqRys75559Np5qUFVcNbvat9Ru/xP4lfo9mRaLEZbL7SZX1G8pJW4p3z4o+Yeo7XezdlnsJPBZBCnUT2EzIAAAAAAAAAAAAAAAAAAAAABzrtDiZ1MZVlFXjTtTi93dTvn+JyN+x2JVKnUqvPUhKVuNle3iaHovE7W4zbbblKK1ldu7yTu8+QF3AaSaspZbPq/9yMpRnCeex2WayfwrbY8aNOnUTtqvjbauq/VB6Oad4Nr6fsvq9ljdFiomm03LJ/eb39eDIRh8/X1ROTcrX22Sf0/JEoR9fP6plR6Ul6+f6lylK3rh/Bmt6e7U4XBK1SetU2qlDvVOKuvsJ55yscx7Rdu8Vi04Rl7ii79yDes1s71Ta+isupndV1HT/b3BYaSpyk6tRNa0KVpaudm3JtJPle5kNC9qcHitX3OIg5bqdRatRPlGVm3zVzg+gNCYjFy1MPScllrTfdpR2bZ7N6yV3yOqdnOwGEwiWJxcoVakLS1p9yhTtnrKLebTS70vBImK6BxtZyt3U8k3za8CFJSazjqtZWumnbemtxzvtD7U4RepgqaqtPOrUuqXNRjk5b83bxLWg/afh6soU61GrSqydo6idaMnyUVr/6X1LUbtCk1d3tfW2dT5RxD1pKW7fbx4Z5E6dJRvqxSvtSb1fBbEEnrPJars737yy3p9N3EqPaNRPYyRRnF37rzz8t/U9Y4pZJp7M3bIm4qyCEKiexpkzIAAAAAAAAAAAAAB8k7ZvJLa9x9Nf7X4jVhSi21CU3r23pLJPlcCGkccsRGdKLlGlLJzSWtLon9n5vkY3CaKqUv8Udzj+a3GQwUFZWs1uaMjSjbYVWMVFSs2s1seyS6SWa8CxCcobW5R33+NLjl8S5beb2F6VNPak+e8h7hbm11zKjzrU75rh5rc/oYrT2CrV6Lp4fEPDzbvrqKk2rN6vGN/vLPIy9KlKLsrSg9lnnF9H9nlu+n2rh3tSafTLxFHDcf2E0jTm17l1tdu1SnLXi396TlZrrJJG29nfZhGOrUx09eW33MG1TSTXxTyct2yy6m/a0o7Vdcv06fQqaTwdPFUZ0Krl7udtfVk4SsnxWfDJ5PgINd0326weCj7jDxhVnFWVOnaNCL2d6aVt265zPtB2mxOMlrYipeKbcace7Sj4b3zd2bZpX2WyTvha8Wsu5Wyazt8cVZpfhRn+zvYHC4Re+xEo1qsU5OU0lRhbbJQfBq95bOROq0zsl2JxOM1alvc0Hb+smruS/wQ+1+J2XU6XRw+jdDUlN6sZNfFLv4io9to7+Oy0Uaz2k9qEI3p4JKctjrSV6a/BH7eex7OpzariK2Iq60nUrV6jSV+/OTzskvF5LJDmI3PtL7RsTibww+thqOfwv+vkv8U18G3ZHzZa7H9ptMVpalHUxNNNKU68bQjlnetGzb5PWfIsdlvZu3q1cc+aoReX/kktvRZc2dHw9KEIxhTjGEIpasYq0Ut1l5ouZo91mlrJXsr2zV99t5CaHr19D6aEdURbWxslYnqgRWInxT6r9D0p4t/aXiv0IM85SJMGRTvmj6YHRVeaxlele9N0MPUS+7PXqwl01oxh+wzPGNAAAAAAAAFeeNprJzjfqYPtlVjLCyqQcZuk1NxT77gvjUVtclFuSW9xS3ktKKCm1G6d+8rWj4FMgxuhsb3YzpzUoSScWs4tPebLhsYntXkc5xuiMTgakq2Cj7zDyblVwrecW9rpcFtyXk8rZXQfa7DVrR1/dVL2dOr3JX4JvJ+DvyGK3+E09jJpGJo1S5TrMqLXu0/Xr11Z8dB/Zk169fPes4KqTVTqUecveLalLwT9bvlxKtWVN/HTafFOzL/vPXrx8z5KSe39fr6v1YGP8Ad07ZTnzur7uXL6EZYdO695CSeUlJWumrNNZ36ci3OhB7l4Zev4p7netPDLc39P5bPCz+7mGs6V9n2DrXapKlN/aoy1Fs26nw7eRd7N9lKOCi1Sg3UatOrJqVR5cfsq62Kxlv6Pz9Z7l4+Ul90kqcuL88/X523SyCcVLg/wCf8SSb4Py47V4Mitf08vN7ufBp7pE1Ofr1yf8Aq+7nfofU3v8AXH9Sd/XrifFUnw+Xr0nyv6Kc+HyXr0uJaPil18vmSs+DJLX9evWXMjXmoRc6k4xitspSUYrq3s/mSiLg+SIOntbeS2t5Jc7mJxPaWDssNTlXb2VG/d4ZZZSdVpuS5wjIyeB0dKaU8TONVvNQgnHDrh3W71HsznfNXSiShoWkpSqYnVSVRU4U3a0pQg5tTe+zc5WXBJ/aMsAQAAAAAAAAedahGeUop9UUquhqT2a0ej/UyIAwGK7OyaahW1XuvC/0aNG0/wCzPG125f0jDSe5yU4y6XSZ1gEiuI4L2f6dw+VGvRSWxRrzUf2ZQ1fkZfDYLtNT3Yap+KdL8lE6uCo5rSxPaRfFgsDLpUSfn778i7DSGnN+jMM+mKjH82b6ArR1pHTP910P3yH6B6R0z/ddD99h/tN4AGiPSumV/ZEX0xtH/wCrFetpnTC2aGl+90X9HyOhgDmU+0+lY/FoXEf5ail/6xfBeRTn2/x0cpaFxi/y1LeapHWQByWXtOxC26KxV+lT/i6+Y/6sVV/ZeI85/wDF0OtXFwjkT9q+K+xoqtydqj+lPkvJHnL2jaYqO1HRk4rnhq0n55L5HYbi4Vx3+ndo8Q84YinF7owhRX7Ulf5l7R3YrHVJKpiI09dbJVqrrzXTOVvBo6oCDAYDs3q/92q5vhFaq/N/QztOCilFKyWSRIFQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB/9k='
          }
          width={500}
          height={500}
          style={{
            width: '100%',
            height: '100%',
          }}
          alt='Image'
        />
      </div>
      <div className={styles.content}>
        <h1>{product?.title || 'Sopa Mask'}</h1>
        <span>Color:{product?.color || 'Black'}</span>
        <span>Size:{product?.size || 'S'}</span>
        <div className={styles.amount}>
          <button
            disabled={product.amount === 1}
            onClick={() => handleCartProductAmount('increment')}
          >
            -
          </button>
          <span>{product.amount}</span>
          <button
            disabled={product.amount === 10}
            onClick={() => handleCartProductAmount('decrement')}
          >
            +
          </button>
        </div>
        <Button
          className={styles.button}
          label='Remove'
          variant='common'
          onClick={() => setCart(removeProduct(cart, product))}
        />
        <span className={styles.total}>
          Subtotal:${product.price * product.amount}
        </span>
      </div>
    </div>
  );
};

export default CartElement;
