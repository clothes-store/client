import React, { useState } from 'react';
import Image from 'next/image';
import { Swiper as SwiperType } from 'swiper';

import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import './index.scss';
import { EffectFade, Navigation, Thumbs } from 'swiper/modules';
const SpecificProductSlider = ({ slides }: { slides: string[] }) => {
  const [thumbsSwiper, setThumbsSwiper] = useState<SwiperType | null>(null);
  return (
    <div className='container'>
      <Swiper
        onSwiper={setThumbsSwiper}
        modules={[Thumbs]}
        freeMode={true}
        watchSlidesProgress={true}
        direction='vertical'
        slidesPerView={slides.length}
        spaceBetween={10}
        wrapperClass='thumbs_wrapper'
        className='thumbs_container'
      >
        {slides.map((src: string, idx: number) => (
          <SwiperSlide key={idx} className='thumb'>
            <Image
              width={200}
              height={700}
              src={`${process.env.NEXT_PUBLIC_SERVER_BASE_URL}/${src}`}
              alt={src}
            />
          </SwiperSlide>
        ))}
      </Swiper>
      <Swiper
        wrapperClass='slider_wrapper'
        className='slider_container'
        slidesPerView={1}
        modules={[Navigation, Thumbs, EffectFade]}
        effect='fade'
        navigation={{
          prevEl: '.prev',
          nextEl: '.next',
          disabledClass: 'button__disabled',
        }}
        thumbs={{
          swiper: thumbsSwiper,
          slideThumbActiveClass: 'thumb_active',
        }}
      >
        {slides.map((src: string, idx: number) => (
          <SwiperSlide key={idx} className='slide'>
            <div className='control prev'>&#x3c;</div>
            <Image
              width={700}
              height={700}
              src={`${process.env.NEXT_PUBLIC_SERVER_BASE_URL}/${src}`}
              alt={src}
            />
            <div className='control next'>&#62;</div>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default SpecificProductSlider;
