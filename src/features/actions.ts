import { CartProduct } from '@/entities/CartProduct';

const addProduct = (cart: CartProduct[], product: CartProduct) => {
  const existedIndex: number = cart.findIndex((el: CartProduct) => {
    return el.id === product.id && product.size === el.size;
  });
  if (existedIndex >= 0) {
    cart[existedIndex].amount += 1;
  } else {
    const newProduct: CartProduct = { ...product, amount: 1 };
    return [...cart, newProduct];
  }
};
const removeProduct = (
  cart: CartProduct[],
  product: CartProduct,
): CartProduct[] => {
  return cart.filter(
    (el: CartProduct) => el.id !== product.id || el.size !== product.size,
  );
};

export { addProduct, removeProduct };
