import { atomWithStorage } from 'jotai/utils';
interface CartItem {
  id: number;
  title: string;
  price: number;
  color: string;
  size: string | null;
  image: string;
}
const cartAtom = atomWithStorage<CartItem[] | any>('cart', []);
export { cartAtom };
