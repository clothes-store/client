import './globals.css';
import '../styles/variables.scss';
import { noto, poppins } from '@/app/fonts';
import Header from '@/components/templates/Header/Header';
import Footer from '@/components/templates/Footer/Footer';
import { Provider } from 'jotai';

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang='en'>
      <body
        className={noto.variable + ' ' + poppins.variable}
        suppressHydrationWarning={true}
      >
        <Provider>
          <Header />
          <main>{children}</main>
          <Footer />
        </Provider>
      </body>
    </html>
  );
}
