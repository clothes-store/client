import { Noto_Serif_Lao, Poppins } from 'next/font/google';

export const noto = Noto_Serif_Lao({
  subsets: ['latin'],
  display: 'swap',
  variable: '--font-noto',
});

export const poppins = Poppins({
  subsets: ['latin'],
  weight: '400',
  display: 'swap',
  variable: '--font-poppins',
});
