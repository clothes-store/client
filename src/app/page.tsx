import HomeSlider from '@/components/HomeSlider/HomeSlider';

import styles from './index.module.scss';
import Image from 'next/image';
import { Button } from '@/components/UI/Button/Button';
import ProductsList from '@/components/templates/ProductsList/ProductsList';
export default function Home() {
  return (
    <>
      <div className={styles.hero}>
        <HomeSlider />
      </div>
      <div className={styles.partners}>
        <h3 className={styles.partners__title}>
          “These are the most thoughtfully designed sneakers on the market.”
        </h3>
        <ul className={styles.partners__list}>
          <li className={styles.partner}>
            <Image src='/vogue.svg' alt='Vogue logo' width='100' height='50' />
          </li>
          <li className={styles.partner}>
            <Image
              src='/esquire.svg'
              alt='Esquire logo'
              width='100'
              height='50'
            />
          </li>
          <li className={styles.partner}>
            <Image
              src='/HUMANS OF NEW YORK.svg'
              alt='Humans of New York logo'
              width='100'
              height='50'
            />
          </li>
        </ul>
      </div>
      <ProductsList />
      <div className={styles.hype}>
        <div className={styles.hype__content}>
          <div className={styles.hype__card}>
            <h4 className={styles.overview__title}>The Hype is real...</h4>
            <div className={styles.card__image}>
              <Image
                src='/Photo.jpg'
                alt='Photo'
                width={500}
                height={500}
                style={{
                  width: '100%',
                  height: '100%',
                  objectFit: 'cover',
                }}
              />
            </div>
            <div className={styles.overview__content}>
              <div className={styles.overview__rating}>
                <span className={styles.overview__author}>bokrat.u.</span>
                <div className={styles.overview__stars}>
                  <span>
                    <Image
                      width={10}
                      height={10}
                      style={{ width: '100%', height: '100%' }}
                      src='/Vector.svg'
                      alt='Star'
                    />
                  </span>
                  <span>
                    <Image
                      width={10}
                      height={10}
                      style={{ width: '100%', height: '100%' }}
                      src='/Vector.svg'
                      alt='Star'
                    />
                  </span>
                  <span>
                    <Image
                      width={10}
                      height={10}
                      style={{ width: '100%', height: '100%' }}
                      src='/Vector.svg'
                      alt='Star'
                    />
                  </span>
                  <span>
                    <Image
                      width={10}
                      height={10}
                      style={{ width: '100%', height: '100%' }}
                      src='/Vector.svg'
                      alt='Star'
                    />
                  </span>
                  <span>
                    <Image
                      width={10}
                      height={10}
                      style={{ width: '100%', height: '100%' }}
                      src='/Vector.svg'
                      alt='Star'
                    />
                  </span>
                </div>
                <div className={styles.overview__text}>
                  These stylishly simple and incredibly comfortable shoes have
                  become such a staple in my daily wardrobe that I`&apos`m
                  already buying a second pair.
                </div>
              </div>
            </div>
          </div>
          <div className={styles.hype__card}>
            <div className={styles.card__image}>
              <Image
                src='/Photo.png'
                alt='Photo'
                width={500}
                height={500}
                style={{
                  width: '100%',
                  height: '100%',
                }}
              />
            </div>
            <div className={styles.card__footer}>
              <div className={styles.card__description}>
                <span className={styles.card__title}>Model 000</span>
                <span className={styles.card__subtitle}>Navy</span>
              </div>
              <Button label='Shop Now' variant='primary' />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
