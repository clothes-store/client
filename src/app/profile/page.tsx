'use client';
import { useEffect, useState } from 'react';
import { localStorageService } from '@/utils/LocalStorage.service';
import { redirect, useRouter } from 'next/navigation';

const Page = () => {
  const router = useRouter();
  const [user, setUser] = useState(null);
  useEffect(() => {
    const fetchUserData = async () => {
      const token = localStorageService.get('accessToken');
      if (!token) {
        router.push('auth/sign-in');
        throw new Error('Token is missing! Please sign in again!');
      }
      try {
        const res = await fetch(
          `${process.env.NEXT_PUBLIC_SERVER_BASE_URL}/users/profile`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
            credentials: 'include',
          },
        );
        if (!res.ok) router.push('auth/sign-in');
        const user = await res.json();
        setUser(() => user);
      } catch (e) {
        throw e;
      }
    };
    fetchUserData();
  }, [router]);
  const signOut = async (e: any) => {
    e.preventDefault();
    const token = localStorageService.get('accessToken');
    if (token) {
      await fetch(`${process.env.NEXT_PUBLIC_SERVER_BASE_URL}/auth/sign-out`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
        credentials: 'include',
      });
      localStorageService.clear('accessToken');
      router.push('/');
    }
    router.push('/');
  };
  return (
    user && (
      <div>
        <button onClick={(e) => signOut(e)}>Sign out</button>
      </div>
    )
  );
};

export default Page;
