import React, { ReactNode } from 'react';
import AuthForm from '@/components/AuthForm/AuthForm';
// import Input from '@/components/UI/InputRHF/Input';
import { Input } from '@/components/UI/Input/Input';
import { Button } from '@/components/UI/Button/Button';
interface Props {
  children: ReactNode[];
}
const Auth = async ({ children }: any) => {
  let child = children as any;
  const path = child.props.childPropSegment;
  const str = path
    .split('-')
    .map((word: string) => word.charAt(0).toUpperCase() + word.slice(1))
    .join(' ');
  return (
    <AuthForm action={path}>
      <Input name='email' />
      <Input name='password' />
      {children}
      <Button label='Submit' variant='common' type='submit' />
    </AuthForm>
  );
};

export default Auth;
