import React from 'react';
import Link from 'next/link';

const Page = () => {
  return (
    <div>
      Don't have an account? <Link href={'sign-up'}>Sign Up</Link>
    </div>
  );
};

export default Page;
