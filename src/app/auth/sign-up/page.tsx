import React from 'react';
import Link from 'next/link';

const Page = () => {
  return (
    <>
      <p>
        Already have an account? <Link href={'sign-in'}>Sign In</Link>
      </p>
    </>
  );
};

export default Page;
