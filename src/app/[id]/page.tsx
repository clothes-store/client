import styles from './specificProduct.module.scss';
import SpecificProductPage from '@/components/SpecificProductPage/SpecificProductPage';
import ReviewList from '@/components/ReviewList/ReviewList';
import React from 'react';

async function getProduct(id: number) {
  const url = `${process.env.NEXT_PUBLIC_SERVER_BASE_URL}/products/${id}`;
  const res = await fetch(url);
  return await res.json();
}
const Page = async ({ params }: { params: { id: number } }) => {
  const product = await getProduct(+params.id);
  return (
    <div className={styles.wrapper}>
      <SpecificProductPage data={product} />
      <div className={styles.rating}>
        <div className={styles.rating_title}>Ratings and Reviews</div>
        <div className={styles.rating_content}>
          <div className={styles.rating_numbers}>
            <span>4.7</span>
            <div className={styles.stars}>
              <img src='/star.svg' alt='Star Icon' />
              <img src='/star.svg' alt='Star Icon' />
              <img src='/star.svg' alt='Star Icon' />
              <img src='/star.svg' alt='Star Icon' />
              <img src='/star.svg' alt='Star Icon' />
            </div>
            <div className={styles.rating_rewiewsAmount}>
              Based on 3,205 reviews
            </div>
          </div>
          <div className={styles.rating_line}>
            <div className={styles.line}>
              <span className={styles.line_poor}>Poor</span>
              <span className={styles.line_perfect}>Perfect</span>
            </div>
            <div className={styles.circle}></div>
          </div>
        </div>
      </div>
      <ReviewList />
    </div>
  );
};

export default Page;
