import React from 'react';
import Filter from '@/components/Filter/Filter';
import styles from '@/app/shop/layout.module.scss';

const Layout = ({ children }: { children: React.ReactNode }) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.overflow}></div>
      {children}
    </div>
  );
};

export default Layout;
