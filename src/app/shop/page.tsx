import Filter from '@/components/Filter/Filter';
import styles from './index.module.scss';
import ShopProductList from '@/components/ShopProductList/ShopProductList';

type PageProps = {
  params: { slug: string };
  searchParams?: { [key: string]: string | string[] | undefined };
};
const fetchPrices = async () => {
  const res = await fetch(
    `${process.env.NEXT_PUBLIC_SERVER_BASE_URL}/products/prices`,
  );
  return await res.json();
};
const Page = async (props: PageProps) => {
  const { searchParams } = props;
  const prices = await fetchPrices();
  return (
    <div className={styles.wrapper}>
      <h2 className={styles.title}>{searchParams?.cat || 'All'}</h2>
      <Filter prices={prices} />
      <ShopProductList searchParams={searchParams} />
    </div>
  );
};

export default Page;
