export interface CartProduct {
  id: number;
  title: string;
  image: string;
  size: string | null;
  color: string;
  price: number;
  amount: number;
}
