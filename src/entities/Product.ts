export interface Product {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  title: string;
  price: number;
  color: string;
  material: string;
  manufacturer: string;
  description: string;
  images: string[];
  sizes: any[];
}
